package com.telkomsel.mytelkomsel.core.eventqueue;

public class Message {

    public enum MessageType {EVENT, ACTION}

    private MessageType type;
    private String name;
    private Object data;
    private boolean lazyProcess;
    private boolean done;
    private int maxRetry = 100;
    private int numOfRetry = 0;

    public Message(MessageType type, String name) {
		this(type, name, null);
	}

	public Message(MessageType type, String name, Object data) {
		this.type = type;
		this.name = name;
		this.data = data;
		this.lazyProcess = false;
		this.done = false;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public boolean isLazyProcess() { return lazyProcess; }

	public void setLazyProcess(boolean lazyProcess) { this.lazyProcess = lazyProcess; }

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	@Override
	public String toString() {
		return "{ type: " + type + 
			", name : " + name +
			", data : " + data +
			", lazyProcess : " + lazyProcess +
			", done : " + done +
			" }";
	}

	public int getMaxRetry() { return maxRetry; }
	public void setMaxRetry(int maxRetry) { this.maxRetry = maxRetry; }
	
	public int getNumOfRetry() { return numOfRetry; }
	public void setNumOfRetry(int numOfRetry) { this.numOfRetry = numOfRetry; }

	public void retry() {
		if (maxRetry <= 0) return;//unlimited.. prevent using this to avoid deadlock
		numOfRetry ++;
		if (numOfRetry >= maxRetry) {
			done = true;
		}
	}

}
