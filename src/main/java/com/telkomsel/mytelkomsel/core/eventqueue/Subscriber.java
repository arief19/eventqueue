/*
 *	Created by ♠ on 2021/11/25 13:48:01
 *	Copyright (c) 2021 . All rights reserved.
 *
 *	Used to wrap processor and support massage filtering to specified type only
 */

package com.telkomsel.mytelkomsel.core.eventqueue;

import java.util.ArrayList;
import java.util.List;

public class Subscriber implements EventQueue.IProcessor {

	private EventQueue.IProcessor processor = null;
	private List<Class<?>> filters = null;

	public Subscriber(EventQueue.IProcessor processor, Class<?> filter) {
		if (filter != null) {
			this.filters = new ArrayList<>();
			this.filters.add(filter);
		}
		init(processor, filters);
	}

	public Subscriber(EventQueue.IProcessor processor, List<Class<?>> filters) {
		init(processor, filters);
	}

	private void init(EventQueue.IProcessor processor, List<Class<?>> filters) {
		this.processor = processor;
		this.filters = filters;
	}

	private boolean isValid(Message message) {
		if (filters == null || filters.size() == 0) return true; //this processor receive all type all message
		for (Class<?> cls : filters) {
			boolean assignable = cls.isAssignableFrom(message.getClass());
			if (assignable) return true;
		}
		return false;
	}

	@Override
	public void process(Message message) {
		boolean validMessage = isValid(message);
		printLog("process[isValid : " + validMessage + "] : " + message);

		if (!validMessage) return; //this message is not for me
		if (processor == null) return; //no processor

		processor.process(message); //process this message
	}

	private void printLog(String log) { }


}
