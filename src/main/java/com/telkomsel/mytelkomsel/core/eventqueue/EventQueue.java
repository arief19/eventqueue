/*
 *	Created by ♠ on 2021/03/26 13:51:20
 *	Copyright (c) 2021 . All rights reserved.
 */

package com.telkomsel.mytelkomsel.core.eventqueue;

import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;
import java.util.List;

public class EventQueue implements Runnable {

    private static EventQueue instance = null;

    public static EventQueue getInstance() {
        if (instance == null) {
            instance = new EventQueue();
        }

        return instance;
    }

    private final Thread thread;
    private final Object lock = new Object();
    private final List<Message> messages;
    private final List<Message> immediateMessages;
    private final List<EventQueue.IProcessor> processors;
    private boolean processing = false;

    private EventQueue() {
        printLog("Created...");

        messages = new ArrayList<>();
        immediateMessages = new ArrayList<>();
        processors = new ArrayList<>();

        thread = new Thread(this);
		thread.start();
	}

	public void addProcessor(EventQueue.IProcessor processor) {
		if(processors.contains(processor)) return;

		processors.add(processor);
	}

	public void removeProcessor(EventQueue.IProcessor processor) {
		processors.remove(processor);
	}

	public void send(Message.MessageType type, String name) {
		send(type, name, null);
	}

	public void send(Message.MessageType type, String name, Object data) {
		Message message = new Message(type, name, data);
		send(message);
	}

	public void send(Message message) {
		//add message to queue
		printLog("send : " + message);
		if(message.isLazyProcess()) {
			synchronized(lock) {
				messages.add(message);

				printLog("notify");
				lock.notifyAll();
			}
		} else {
			immediateMessages.add(message);
			process();
		}
	}
	
	private synchronized void process() {
		printLog("process[processing : " + processing + ", immediateMessages : " + (immediateMessages == null ? "NULL" : immediateMessages.size()) + "]");
		if(processing) return;
		processing = true;
		
		while(immediateMessages.size() > 0) {
			Message message = immediateMessages.get(0);
			immediateMessages.remove(0);
			doProcess(message);
		}
		
		processing = false;
	}

	@Override
	public void run() {
		while(true) {
			printLog("EventQueue is running...");

			synchronized(lock) {
				boolean hasMessage = messages.size() > 0;
				try {
					if(!hasMessage) {
						printLog("processor.waiting....................");
						lock.wait();
					}

					printLog("run[messages : " + (messages == null ? "NULL" : messages.size()) + "]");
					while(messages.size() > 0) {
						Message message = messages.get(0);
						messages.remove(0);
						new Handler(Looper.getMainLooper()).post(() -> doProcess(message));
					}
				} catch(Exception exc) {
					exc.printStackTrace();
					printLog("Exception caught on EventQueue : " + exc.getMessage());
				}

				printLog("processor.notify");
				lock.notifyAll();
			}
		}
	}

	private synchronized void doProcess(final Message message) {
		printLog("doProcess[message : " + message + ", processors : " + (processors == null ? "NULL" : processors.size()) + "]");
		if(processors == null) return;
		for(int i=0; i<processors.size(); i++) {
			IProcessor processor = processors.get(i);
			processor.process(message);
		}

		//auto done for EVENT type
		if(message.getType() == Message.MessageType.EVENT) {
			message.setDone(true);
		}

		printLog(message + "[" +
			"type : " + message.getType() + 
			", isDone : " + message.isDone() + 
			", maxRetry : " + message.getMaxRetry() +
			", numOfRetry : " + message.getNumOfRetry() + 
		"]");

		if(message.isDone()) return;
		message.retry();
		new Thread(() -> {
			try { Thread.sleep(1000); }
			catch(Exception ignored) { }
			new Handler(Looper.getMainLooper()).post(() -> send(message));
		}).start();
	}

	private void printLog(String log) { }

	public interface IProcessor {
		void process(Message message);
	}
}
